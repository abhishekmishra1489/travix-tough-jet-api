package com.travix.medusa.toughjet.controller;

import com.travix.medusa.toughjet.model.ToughJetRequest;
import com.travix.medusa.toughjet.model.ToughJetRequestList;
import com.travix.medusa.toughjet.model.ToughJetResponseList;
import com.travix.medusa.toughjet.service.ToughJetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/tough-jet")
public class ToughJetController {

    private ToughJetService toughJetService;

    @Autowired
    public ToughJetController(ToughJetService toughJetService) {
        this.toughJetService = toughJetService;
    }

    @PostMapping("/flights-detail")
    public ResponseEntity<?> getToughJetFlightDetails(@RequestBody ToughJetRequest toughJetRequest) {
        ToughJetResponseList toughJetResponses = toughJetService.retrieveFlightDetails(toughJetRequest);
        return new ResponseEntity<>(toughJetResponses, HttpStatus.OK);
    }


}
