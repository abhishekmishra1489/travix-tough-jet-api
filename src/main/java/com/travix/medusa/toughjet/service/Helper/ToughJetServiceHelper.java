package com.travix.medusa.toughjet.service.Helper;

import com.travix.medusa.toughjet.model.ToughJetRequest;
import com.travix.medusa.toughjet.model.ToughJetResponse;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class ToughJetServiceHelper {

    private static final String TOUGH_JET_CARRIER = "Tough Jet";

    /**
     * @return
     */
    public Function<ToughJetRequest, ToughJetResponse> getToughJetResponses() {
        return toughJetRequest -> {
            ToughJetResponse response =
                    ToughJetResponse.builder()
                            .carrier(TOUGH_JET_CARRIER)
                            .basePrice(400)
                            .tax(8)
                            .discount(4)
                            .departureAirportName(toughJetRequest.getFrom())
                            .arrivalAirportName(toughJetRequest.getTo())
                            .outboundDateTime(toughJetRequest.getOutboundDate())
                            .inboundDateTime(toughJetRequest.getInboundDate())
                            .build();
            return response;
        };
    }
}
