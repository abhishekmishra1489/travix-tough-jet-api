package com.travix.medusa.toughjet.service;

import com.travix.medusa.toughjet.model.ToughJetRequest;
import com.travix.medusa.toughjet.model.ToughJetResponse;
import com.travix.medusa.toughjet.model.ToughJetResponseList;
import com.travix.medusa.toughjet.service.Helper.ToughJetServiceHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class ToughJetService {

    private ToughJetServiceHelper toughJetServiceHelper;

    @Autowired
    public ToughJetService(ToughJetServiceHelper toughJetServiceHelper) {
        this.toughJetServiceHelper = toughJetServiceHelper;
    }

    public ToughJetResponseList retrieveFlightDetails(ToughJetRequest toughJetRequest) {
        ToughJetResponse toughJetResponse1 =
                ToughJetResponse.builder()
                        .carrier("SWISS")
                        .departureAirportName(toughJetRequest.getFrom())
                        .arrivalAirportName(toughJetRequest.getTo())
                        .outboundDateTime(toughJetRequest.getOutboundDate())
                        .inboundDateTime(toughJetRequest.getInboundDate())
                        .basePrice(320)
                        .tax(7)
                        .discount(4)
                        .build();
        ToughJetResponse toughJetResponse2 =
                ToughJetResponse.builder()
                        .carrier("Lufthansa")
                        .departureAirportName(toughJetRequest.getFrom())
                        .arrivalAirportName(toughJetRequest.getTo())
                        .outboundDateTime(toughJetRequest.getOutboundDate())
                        .inboundDateTime(toughJetRequest.getInboundDate())
                        .basePrice(161)
                        .tax(7)
                        .discount(4)
                        .build();
        ToughJetResponse toughJetResponse3 =
                ToughJetResponse.builder()
                        .carrier("KLM")
                        .departureAirportName(toughJetRequest.getFrom())
                        .arrivalAirportName(toughJetRequest.getTo())
                        .outboundDateTime(toughJetRequest.getOutboundDate())
                        .inboundDateTime(toughJetRequest.getInboundDate())
                        .basePrice(512)
                        .tax(7)
                        .discount(4)
                        .build();
        ToughJetResponse toughJetResponse4 =
                ToughJetResponse.builder()
                        .carrier("AIR FRANCE")
                        .departureAirportName(toughJetRequest.getFrom())
                        .arrivalAirportName(toughJetRequest.getTo())
                        .outboundDateTime(toughJetRequest.getOutboundDate())
                        .inboundDateTime(toughJetRequest.getInboundDate())
                        .basePrice(219)
                        .tax(7)
                        .discount(4)
                        .build();
        return ToughJetResponseList.builder()
                .toughJetResponseList(Arrays.asList(toughJetResponse1, toughJetResponse2, toughJetResponse3,toughJetResponse4)).build();
    }
}
