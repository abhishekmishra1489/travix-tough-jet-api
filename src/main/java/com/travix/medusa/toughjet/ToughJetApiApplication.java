package com.travix.medusa.toughjet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class ToughJetApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(ToughJetApiApplication.class, args);
    }

}
