package com.travix.medusa.toughjet.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ToughJetResponseList {
    private List<ToughJetResponse> toughJetResponseList;
}
