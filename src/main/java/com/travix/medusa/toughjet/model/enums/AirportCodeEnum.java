package com.travix.medusa.toughjet.model.enums;

public enum AirportCodeEnum {
    LHR("LONDON"),
    AMS("AMSTERDAM"),
    MUC("MUCNICH"),
    ZSH("ZURICH"),
    NCE("NICE");

    String value;

    AirportCodeEnum(String value) {
        this.value = value;
    }

}
